package com.testeoF.demotestF;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.when;

import java.util.stream.Stream;
import java.util.stream.Collectors;
import com.testeoF.controller.MensajeController;
import com.testeoF.module.Mensaje;
import com.testeoF.repo.MensajeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTestFApplicationTests {
	@Autowired
	private MensajeController mensajeController;
	
	@MockBean
	private MensajeRepository mensajeRepository;
	
	@Test
	public void getMensajesTest() {
		when(mensajeRepository.findAll()).thenReturn(Stream
				.of(new Mensaje(1234,"1test","test1"),new Mensaje(4321, "2test", "test2")).collect(Collectors.toList()));
		assertEquals(2, mensajeController.getAllMensajes().size());
	}
	
	@Test
	public void saveMensajeTest() {
		Mensaje mensaje=new Mensaje (432,"123","textoTest");
		when(mensajeRepository.save(mensaje)).thenReturn(mensaje);
		assertEquals(mensaje, mensajeController.createMensaje(mensaje));
	}

	
	

}
