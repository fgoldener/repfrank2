package com.nuevoTestD.demodrools2;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class MessageController {
	@Autowired
	private KieSession session;
	
	 @PostMapping("/mess")
	public Message messageNow(@RequestBody Message mess) {
		session.insert(mess);
		session.fireAllRules();
		return mess;
	}
}
