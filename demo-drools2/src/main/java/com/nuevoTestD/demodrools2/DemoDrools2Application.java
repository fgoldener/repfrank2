package com.nuevoTestD.demodrools2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDrools2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoDrools2Application.class, args);
	}

}
